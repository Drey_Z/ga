﻿using Cluster.Problems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cluster
{
    public class Chromosome : IComparer<Chromosome>, IFit
    {
        #region Fields

        private Random _random;

        public static IComparer<Chromosome> __Comparer { get; set; }

        internal static VariableRepresentation __VariableRepresentation { get; set; }
        internal static Func<Chromosome, Chromosome, int> __CompareChromosomesFunction { get; set; }

        public Func<bool[], double> FitFunction { get; private set; }

        public double[] Coordinates { get; private set; }

        public bool[] Genes { get; private set; }
        public double FitValue { get; private set; }

        public int Generation { get; set; } = 0;

        #endregion Fields

        #region Properties

        // public Func<bool> GenerateRangodmGene { get; }

        #endregion Properties

        public Chromosome(
            // Func<bool> generateRangodmGene,
            Func<bool[], double> fitFunction,
            int size,
            Random random,
            bool[] genes = null)
        {
            FitFunction = fitFunction;
            _random = random;
            // GenerateRangodmGene = generateRangodmGene;

            if (genes == null)
            {
                Genes = new bool[size];
                Initialize();
            }
            else
            {
                Genes = genes;
            }
        }

        internal void CalculateCoordinates()
        {
             Coordinates = new double[__VariableRepresentation.Dimension + 1];

            var variableCoordinates = __VariableRepresentation.ConvertGenesToDoobleGeneDictionaryValues(Genes).Keys.ToList();

            for (int i = 1; i < __VariableRepresentation.Dimension; i++)
            {
                Coordinates[i] = variableCoordinates[i - 1];
            }

            Coordinates[0] = FitValue;
        }

        #region Public Methods

        public double CalculateFintess()
        {
            FitValue = FitFunction(Genes);
            return FitValue;
        }

        public void Initialize()
        {
            for (int i = 0; i < Genes.Length; i++)
            {
                Genes[i] = _random.NextDouble() > 0.5; //GenerateRangodmGene();
            }
        }

        public int Compare(Chromosome x, Chromosome y)
        {
            return x.FitValue.CompareTo(y.FitValue);
        }

        public override string ToString()
        {
            var builder = new StringBuilder();

            builder.AppendLine($"Fit value: {FitValue}");
            foreach (var coordinate in Coordinates)
                builder.Append($"{coordinate} ");

            return builder.ToString();
        }

        #endregion Public Methods
    }
}