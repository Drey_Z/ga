﻿using System.Collections.Generic;

namespace Cluster.Problems
{
    public class ExperimentResults
    {
        public long MSTime { get; set; }

        public long Tics { get; set; }

        public double FitValue { get; set; }
        
        public List<double> XCoordinates { get; set; }
    }
}
