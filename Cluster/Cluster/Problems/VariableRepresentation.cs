﻿using Cluster.Extentions;
using System;
using System.Collections.Generic;

namespace Cluster.Problems
{
    // one for all
    internal class VariableRepresentation
    {
        public double MaxDoubleValue { get; private set; }
        public double MinDoubleValue { get; private set; }

        public int MinIntValue { get; set; }
        public int MaxIntValue { get; set; }

        public int Dimension { get; private set; }

        public int Precision { get; private set; }
        public int ChromosomeLength { get; private set; }

        public int VaribaleChromosomeLength {get; private set;}

        private int DomainLength { get; set; }

        private int IntDomainLength { get; set; }

        public VariableRepresentation(double minValue, double maxValue, int precision, int dimension)
        {
            Dimension = dimension;
            MinDoubleValue = minValue;
            MaxDoubleValue = maxValue;
            Precision = precision;

            CalculateDomainLength();
            CalculateChormosomeLength();
            CalculateIntDomainLenth();
        }

        public double ToDoubleValue(bool[] gene)
        {
            int nonNormilized = CalculateGeneIntRepresentation(gene);

            return Normilize(nonNormilized);
        }

        public Dictionary<double, bool[]> ConvertGenesToDoobleGeneDictionaryValues(bool[] genes)
        {
            var doubleGene = new Dictionary<double, bool[]>();
        
            for(int i = 0, j = 0; i < genes.Length; i+= VaribaleChromosomeLength, j++)
            {
                bool[] variableChromosome = new bool[VaribaleChromosomeLength];

                Array.Copy(genes, i, variableChromosome, 0, VaribaleChromosomeLength);

                doubleGene[Math.Round(ToDoubleValue(variableChromosome), Precision)] = variableChromosome;
            }

            return doubleGene;
        }

        // Calculates general length of genes in the chromosome;
        private void CalculateChormosomeLength()
        {
            var variations = DomainLength * Math.Pow(10, Precision);

            VaribaleChromosomeLength = (int)Math.Ceiling(Math.Log(variations, 2));
            ChromosomeLength = VaribaleChromosomeLength * Dimension;
        }

        private void CalculateDomainLength()
        {
            // TODO: Check domain length 
            DomainLength = (int)Math.Round(MaxDoubleValue - MinDoubleValue);
        }

        private void CalculateIntDomainLenth()
        {
            var binaryMin = new bool[VaribaleChromosomeLength];
            var binaryMax = new bool[VaribaleChromosomeLength];

            GeneInitializer(binaryMax, true);
            GeneInitializer(binaryMin, false);

            MinIntValue = CalculateGeneIntRepresentation(binaryMin);
            MaxIntValue = CalculateGeneIntRepresentation(binaryMax);


            IntDomainLength = MaxIntValue - MinIntValue;
        }

        private int CalculateGeneIntRepresentation(bool[] genes)
        {
            var valueRepresentationArray = new bool[VaribaleChromosomeLength];
            Array.Copy(genes, 0, valueRepresentationArray, 0, VaribaleChromosomeLength);
            string strRepresentation = valueRepresentationArray.ConvertFromBoolArray();

            return Convert.ToInt32(strRepresentation, 2);
        }

        private double Normilize(int nonNormilized)
        {
            double slope = 1.0 * (MaxDoubleValue - MinDoubleValue) / (MaxIntValue - MinIntValue);
            var output = MinDoubleValue + slope * (nonNormilized - MinIntValue);
            return output; //((nonNormilized - MinIntValue) * DomainLength / IntDomainLength) + MaxDoubleValue;
        }

        private void GeneInitializer(bool[] genes, bool valueToSet)
        {
            for(int i = 0; i < genes.Length; i++)
            {
                genes[i] = valueToSet;
            }
        }
    }
}
