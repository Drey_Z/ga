﻿using Cluster.Benchmarks;
using Cluster.Extentions;
using Cluster.GA.Crossover;
using Cluster.GA.Mutation;
using Cluster.GA.Selection;
using Cluster.Modifications;
using NLog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Cluster.Problems
{
    internal class ProblemExpression
    {
        private static readonly ILogger _logger = LogManager.GetCurrentClassLogger();

        public int PopulationSize { get; set; }
        private GA.GeneticAlgorithm GA { get; set; }
        private IProblemFunction _problemFunction;
        private Stopwatch _stopWatch;
        private List<ExperimentResults> _experimentResults;
        private List<List<ExperimentResults>> _accomulativeResults;

        internal void RS()
        {
            for(int i = 0; i < _accomulativeResults.Count - 1; i++)
            {
                CalculateRS(_accomulativeResults[i], _accomulativeResults[i + 1]);
            }

            CalculateRS(_accomulativeResults[0], _accomulativeResults[_accomulativeResults.Count-1]);
        }

        private void CalculateRS(List<ExperimentResults> list1, List<ExperimentResults> list2)
        {
            double r = CalculateR(list1, list2);
            double s = CalculateS(list1, list2);

            _logger.Info("S:{S} R:{R}", s, r);
        }

        private double CalculateS(List<ExperimentResults> list1, List<ExperimentResults> list2)
        {
            double sum = 0;
            double minExpCount = Math.Min(list1.Count, list2.Count);
            for (int i = 0; i < minExpCount; i++)
            {
                sum += (list1[i].MSTime - list2[i].MSTime) / Math.Max(list1[i].MSTime, list2[i].MSTime);
            }

            return sum / minExpCount;
        }

        // currenlty works only for 3 experiments!
        private double CalculateR(List<ExperimentResults> list1, List<ExperimentResults> list2)
        {
            double sum = 0;
            double minExpCount = Math.Min(list1.Count, list2.Count);
            for (int i = 0; i < minExpCount; i++)
            {
                sum += list1[i].MSTime - list2[i].MSTime <= 0 ? 0.0 : 1.0;
            }

            return sum / minExpCount;
        }

        public ProblemExpression(IProblemFunction problemFunction)
        {
            _problemFunction = problemFunction;
            _stopWatch = new Stopwatch();
            _experimentResults = new List<ExperimentResults>();
            _accomulativeResults = new List<List<ExperimentResults>>();
            InitializeGeneticAlgorithm();
        }

        internal void Restart()
        {
            _logger.Info(GetAverage);

            GA.SelectionStrategy = SelectSelectionStartegy();

            _accomulativeResults.Add(new List<ExperimentResults>(_experimentResults));
            _experimentResults.Clear();

            Start();
        }

        private string GetAverage()
        {
            var strBuilder = new StringBuilder();

            strBuilder.AppendLine($"Avg Fit: {_experimentResults.Average(e => e.FitValue)};");
            strBuilder.AppendLine($"Avg ms time: {_experimentResults.Average(e => e.MSTime)};");
            strBuilder.AppendLine($"Avg tick: {_experimentResults.Average(e => e.Tics)};");

            return strBuilder.ToString();
        }

        private void InitializeGeneticAlgorithm()
        {
            var random = new Random();

            _logger.Info("Population size:");
            int populationSize = Console.ReadLine().GetIntValue();

            _logger.Info("Mesure operators time:");
            bool mesureOperators = Console.ReadLine().GetAnswValue();

            var selectionStrategy = SelectSelectionStartegy();
            var crossoverOperator = SelectCrossoverStrategy();
            var mutationOperator = ConfigMuationOperator();

            GA = new GA.GeneticAlgorithm(populationSize,
                selectionStrategy,
                crossoverOperator,
                mutationOperator,
                _problemFunction,
                mesureOperators);
        }

        public void Start()
        {
            _logger.Info("Generation number: ");
            int generationNumber = Console.ReadLine().GetIntValue();

            _logger.Info("Times to experiment: ");

            int times = Console.ReadLine().GetIntValue();
            _stopWatch.Start();
            GA.StartGA(generationNumber);
            _stopWatch.Stop();

            LogInfo();            

            for (int i = 0; i < times; i++)
            {
                _logger.Info("Adding garabage...");
                AddGarbage();

                GA.Reset();
                _stopWatch.Restart();

                GA.StartGA(generationNumber);
                _stopWatch.Stop();
                LogInfo();
            }
        }

        private void LogInfo()
        {
            GA.BestChromosomes.Sort(Chromosome.__Comparer);
            _logger.Warn("\n\n {0} #{1}", GA.BestChromosomes[0].FitValue, GA.BestChromosomes[0].Generation);
            _logger.Warn("General time: {0} time, {1} ms, {2} ticks.", _stopWatch.Elapsed, _stopWatch.ElapsedMilliseconds, _stopWatch.ElapsedTicks);
            
            var doubleGene = Chromosome.__VariableRepresentation.ConvertGenesToDoobleGeneDictionaryValues(GA.BestChromosomes[0].Genes);

            LogCoord(doubleGene.Keys.ToList());

            _experimentResults.Add(new ExperimentResults
            {
                FitValue = GA.BestChromosomes[0].FitValue,
                MSTime = _stopWatch.ElapsedMilliseconds,
                Tics = _stopWatch.ElapsedTicks,
                XCoordinates = doubleGene.Keys.ToList()
            });;
        }

        private void LogCoord(List<double> list)
        {
            var strBuilder = new StringBuilder();

            strBuilder.Append("X values: [ ");
            foreach (var gene in list)
            {
                strBuilder.Append($"{gene} ");
            }
            strBuilder.Append("]");
            _logger.Info(strBuilder.ToString());
        }

        private IMutationOperator ConfigMuationOperator()
        {
            _logger.Info("Muation rate: ");
            string rate = Console.ReadLine();

            return new MutationOperator(rate.GetIntValue());
        }

        private ICrossoverOperator SelectCrossoverStrategy()
        {
            _logger.Info("Select crossover to be tested:");
            _logger.Info("1. Domain crossover.");
            _logger.Info("2. Random function.");

            string choice = Console.ReadLine();

            switch (choice)
            {
                case "1": return new TwoToTwoCrossoverOperator(new DomainCrossover());
                case "2": return new TwoToTwoCrossoverOperator(new RandomCrossover());
            }

            return null;
        }

        private ISelectionStrategy<Chromosome> SelectSelectionStartegy()
        {
            _logger.Info("Select selection to be tested:");
            _logger.Info("1. Roulete function.");
            _logger.Info("2. Tournament function.");
            _logger.Info("3. Rouelte maximin clustering.");

            string choice = Console.ReadLine();

            switch (choice)
            {
                case "1": return new RouletteSelection<Chromosome>(_problemFunction.IsMaximization);
                case "2": return new TournamentSelection();
                case "3":
                    return new ClusterSelection(
              null,
              new RouletteSelection<ClusterEntity>(_problemFunction.IsMaximization),
              new RouletteSelection<Chromosome>(_problemFunction.IsMaximization));
            }

            return null;
        }

        private void AddGarbage()
        {
            var random = new Random();
            for (long i = 0; i < 1000000000; i++)
            {
                random.Next();
            }
        }
    }
}
