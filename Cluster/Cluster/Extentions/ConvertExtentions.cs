﻿using NLog;
using System.Linq;

namespace Cluster.Extentions
{
    internal static class ConvertExtentions
    {
        private static readonly ILogger _logger = LogManager.GetCurrentClassLogger();

        public static string ConvertFromBoolArray(this bool[] binaryArray) => string.Join(string.Empty, binaryArray.Select(v => v ? 1 : 0).ToList());

        public static bool[] ConvertFromString(this string binaryString) => binaryString.Select(t => t == '1').ToArray();

        public static double GetDoubleValue(this string stringDoubleValue)
        {
            if (double.TryParse(stringDoubleValue, out double resultValue))
            {
                return resultValue;
            }

             _logger.Info("Warning, provided number is not valid.");
            return default;
        }

        public static int GetIntValue(this string stringIntValue)
        {
            if (int.TryParse(stringIntValue, out int resultValue))
            {
                return resultValue;
            }

             _logger.Info("Warning, provided number is not valid.");
            return default;
        }

        public static bool GetAnswValue(this string stringAnswValue)
        {
            return stringAnswValue == "Y" || stringAnswValue == "y";
        }
    }
}
