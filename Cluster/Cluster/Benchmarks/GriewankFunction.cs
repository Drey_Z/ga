﻿using Cluster.Helpers;
using Cluster.Problems;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Cluster.Benchmarks
{
    internal class GriewankFunction : IProblemFunction
    {
        private static readonly ILogger _logger = LogManager.GetCurrentClassLogger();
        public bool IsParametersSet => true;

        private IComparer<Chromosome> _comparer = new FromMinToMaxComparer();

        public VariableRepresentation VariableRepresentation { get; set; }
        public int Dimensions { get; set; }

        public bool IsMaximization => false;

        public void ConfigureParameters() { }

        public GriewankFunction()
        {
            Chromosome.__Comparer = _comparer;
        }

        public double FitnessFunction(bool[] genes)
        {
            var doubleGene = VariableRepresentation.ConvertGenesToDoobleGeneDictionaryValues(genes);
            _logger.Debug("X values: [");
            foreach (var gene in doubleGene.Keys.ToList())
            {
                _logger.Debug("{0}\t", gene);
            }
            _logger.Debug("]");
            double resultFitValue = CalculateFunction(doubleGene.Keys.ToList());
            _logger.Debug("\tFit Value: {0}", resultFitValue);
            return Math.Round(resultFitValue, VariableRepresentation.Precision);
        }

        private double CalculateFunction(List<double> variablesSet)
        {
            double sum = 0;
            double mult = 0;

            for (int i = 0; i < variablesSet.Count; i++)
            {
                var x = variablesSet[i];

                sum += Math.Pow(x, 2);

                if (i == 0)
                    mult += Math.Cos(x);
                else
                    mult *= Math.Cos(x / Math.Sqrt(i + 1));
            }
            
            return 1.0 + (1.0/4000.0*sum) - mult;
        }
    }
}
