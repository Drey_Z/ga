﻿using Cluster.Extentions;
using Cluster.Helpers;
using Cluster.Problems;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Cluster.Benchmarks
{
    internal class HappyCatFunction : IProblemFunction
    {
        private static readonly ILogger _logger = LogManager.GetCurrentClassLogger();
        public bool IsParametersSet => Alpha != default;
        public double Alpha { get; set; }

        private IComparer<Chromosome> _comparer = new FromMinToMaxComparer();

        public VariableRepresentation VariableRepresentation { get; set; }
        public int Dimensions { get; set; }

        public bool IsMaximization => false;

        public void ConfigureParameters()
        {
            _logger.Info("Set Ackley Function parameters: ");

            _logger.Info("Alpha: ");
            Alpha = Console.ReadLine().GetDoubleValue();
        }
        public HappyCatFunction()
        {
            Chromosome.__Comparer = _comparer;
        }

        public HappyCatFunction(double alpha)
        {
            Alpha = alpha;
            Chromosome.__Comparer = _comparer;
        }

        public double FitnessFunction(bool[] genes)
        {
            var doubleGene = VariableRepresentation.ConvertGenesToDoobleGeneDictionaryValues(genes);
            _logger.Debug("X values: [");
            foreach (var gene in doubleGene.Keys.ToList())
            {
                _logger.Debug("{0}\t", gene);
            }
            _logger.Debug("]");
            double resultFitValue = CalculateFunction(doubleGene.Keys.ToList());
            _logger.Debug("\tFit Value: {0}", resultFitValue);
            return Math.Round(resultFitValue, VariableRepresentation.Precision);
        }

        private double CalculateFunction(List<double> variablesSet)
        {
            double normSumWithPow = variablesSet.Sum(v => Math.Pow(v, 2));
            double simpleSum = variablesSet.Sum();

            double leftPart = Math.Pow(Math.Pow(normSumWithPow - variablesSet.Count, 2), Alpha);
            double rightPart = (1.0 / variablesSet.Count) * (0.5 * normSumWithPow + simpleSum) + 0.5;
            return leftPart + rightPart;
        }
    }
}
