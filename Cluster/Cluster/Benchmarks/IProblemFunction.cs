﻿using Cluster.Problems;

namespace Cluster.Benchmarks
{
    internal interface IProblemFunction
    {
        /// <summary>
        /// Returns true if all parameters configured from the user input.
        /// </summary>
        bool IsParametersSet { get; }

        /// <summary>
        /// Provide unified API for the parameters configuration.
        /// </summary>
        void ConfigureParameters();

        /// <summary>
        /// Calculate fitness function for the problem function
        /// </summary>
        /// <param name="genes"></param>
        /// <returns></returns>
        double FitnessFunction(bool[] genes);

        /// <summary>
        /// Represenation of the variables in the encoded format.
        /// </summary>
        VariableRepresentation VariableRepresentation { get; set; }

        int Dimensions { get; set; }

        bool IsMaximization { get; }

    }
}
