﻿using Cluster.Extentions;
using Cluster.Helpers;
using Cluster.Problems;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Cluster.Benchmarks
{
    internal class AckelyFunction : IProblemFunction
    {
        private static readonly ILogger _logger = LogManager.GetCurrentClassLogger();

        private IComparer<Chromosome> _comparer = new FromMinToMaxComparer();

        public int Dimensions { get; set; }

        public double AParameter { get; set; }
        public double BParameter { get; set; }
        public double CParameter { get; set; }

        public VariableRepresentation VariableRepresentation { get; set; }

        public double Target { get; } = 0;

        public bool IsParametersSet =>
            AParameter != default &&
            BParameter != default &&
            CParameter != default &&
            VariableRepresentation != null;

        public bool IsMaximization => false;

        public AckelyFunction()
        {
            Chromosome.__Comparer = _comparer;
        }

        public AckelyFunction(double a, double b, double c, VariableRepresentation variable)
        {
            AParameter = a;
            BParameter = b;
            CParameter = c * Math.PI;

            VariableRepresentation = variable;
        }

        private double CalculateFunction(List<double> variablesSet)
        {
            double sum = 0;
            double sum2 = 0;

            for (int i = 0; i < variablesSet.Count; i++)
            {
                var x = variablesSet[i];

                sum += Math.Pow(x, 2);
                sum2 += Math.Cos(CParameter * x);
            }

            var term = -AParameter * Math.Exp(-BParameter * Math.Sqrt(sum / Dimensions));
            var term2 = -Math.Exp(sum2 / Dimensions);

            return term + term2 + AParameter + Math.Exp(1);
        }

        public void ConfigureParameters()
        {
            _logger.Info("Set Ackley Function parameters: ");

            _logger.Info("A: ");
            AParameter = Console.ReadLine().GetDoubleValue();

            _logger.Info("B: ");
            BParameter = Console.ReadLine().GetDoubleValue();

            _logger.Info("C: ");
            CParameter = Console.ReadLine().GetDoubleValue();
        }

        public double FitnessFunction(bool[] genes)
        {
            var doubleGene = VariableRepresentation.ConvertGenesToDoobleGeneDictionaryValues(genes);
            _logger.Debug("X values: [");
            foreach (var gene in doubleGene.Keys.ToList())
            {
                _logger.Debug("{0}\t", gene);
            }
            _logger.Debug("]");
            double resultFitValue = CalculateFunction(doubleGene.Keys.ToList());
            _logger.Debug("\tFit Value: {0}", resultFitValue);
            return Math.Round(resultFitValue, VariableRepresentation.Precision);
        }
    }
}
