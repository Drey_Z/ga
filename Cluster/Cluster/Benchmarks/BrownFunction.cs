﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cluster.Helpers;
using Cluster.Problems;
using NLog;

namespace Cluster.Benchmarks
{
    class BrownFunction : IProblemFunction
    {
        private static readonly ILogger _logger = LogManager.GetCurrentClassLogger();
        public bool IsParametersSet => true;

        private IComparer<Chromosome> _comparer = new FromMinToMaxComparer();

        public VariableRepresentation VariableRepresentation { get; set; }
        public int Dimensions { get; set; }

        public bool IsMaximization => false;

        public void ConfigureParameters() { }

        public BrownFunction()
        {
            Chromosome.__Comparer = _comparer;
        }

        public double FitnessFunction(bool[] genes)
        {
            var doubleGene = VariableRepresentation.ConvertGenesToDoobleGeneDictionaryValues(genes);
            _logger.Debug("X values: [");
            foreach (var gene in doubleGene.Keys.ToList())
            {
                _logger.Debug("{0}\t", gene);
            }
            _logger.Debug("]");
            double resultFitValue = CalculateFunction(doubleGene.Keys.ToList());
            _logger.Debug("\tFit Value: {0}", resultFitValue);
            return Math.Round(resultFitValue, VariableRepresentation.Precision);
        }

        private double CalculateFunction(List<double> variablesSet)
        {
            double sum = 0;

            for (int i = 0; i < variablesSet.Count - 1; i++)
            {
                double x = variablesSet[i];
                double xNext = variablesSet[i + 1];
                double xPow = Math.Pow(x, 2);
                double nextXPow = Math.Pow(xNext, 2);

                sum += Math.Pow(xPow, nextXPow + 1) + Math.Pow(nextXPow, xPow + 1);
            }

            return sum;
        }
    }
}
