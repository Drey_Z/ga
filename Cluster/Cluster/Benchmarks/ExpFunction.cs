﻿using Cluster.Helpers;
using Cluster.Problems;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Cluster.Benchmarks
{
    internal class ExpFunction : IProblemFunction
    {
        private static readonly ILogger _logger = LogManager.GetCurrentClassLogger();
        public bool IsParametersSet => true;

        private IComparer<Chromosome> _comparer = new FromMinToMaxComparer();

        public VariableRepresentation VariableRepresentation { get; set; }
        public int Dimensions { get; set; }

        public bool IsMaximization => false;

        public void ConfigureParameters() { }

        public ExpFunction()
        {
            Chromosome.__Comparer = _comparer;
        }

        public double FitnessFunction(bool[] genes)
        {
            var doubleGene = VariableRepresentation.ConvertGenesToDoobleGeneDictionaryValues(genes);
            _logger.Debug("X values: [");
            foreach (var gene in doubleGene.Keys.ToList())
            {
                _logger.Debug("{0}\t", gene);
            }
            _logger.Debug("]");
            double resultFitValue = CalculateFunction(doubleGene.Keys.ToList());
            _logger.Debug("\tFit Value: {0}", resultFitValue);
            return Math.Round(resultFitValue, VariableRepresentation.Precision);
        }

        private double CalculateFunction(List<double> variablesSet)
        {
            double sum = 0;

            for (int i = 0; i < variablesSet.Count; i++)
            {
                double x = variablesSet[i];
                
                sum += Math.Pow(x, 2);
            }

            return -Math.Exp(-0.5 * sum);
        }
    }
}
