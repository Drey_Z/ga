﻿using Cluster.Benchmarks;
using Cluster.Extentions;
using Cluster.Problems;
using NLog;
using System;

namespace Cluster
{
    class Program
    {
        private static readonly ILogger _logger = LogManager.GetCurrentClassLogger();

        static void Main(string[] args)
        {
            _logger.Info("Genetic algorithm hybridization");

            var function = FunctionSelector();
            var variablePresentation = VariableInitializer();

            function.Dimensions = variablePresentation.Dimension;
            function.VariableRepresentation = variablePresentation;

            var problem = new ProblemExpression(function);

            problem.Start();
            problem.Restart();
            problem.Restart();

            problem.RS();

            Console.ReadLine();
        }

        public static IProblemFunction FunctionSelector()
        {
            IProblemFunction problemFunction = default;

            _logger.Info("Select function to be solved:");
            _logger.Info("1. Ackley function.");
            _logger.Info("2. Greiwank function.");
            _logger.Info("3. Qing function.");
            _logger.Info("4. Exp function.");
            _logger.Info("5. Brown function.");
            _logger.Info("6. Happy cat function.");

            string choice = Console.ReadLine();

            switch (choice)
            {
                case "1":
                    problemFunction = new AckelyFunction();
                    break;
                case "2":
                    problemFunction = new GriewankFunction();
                    break;
                case "3":
                    problemFunction = new QingFunction();
                    break;
                case "4":
                    problemFunction = new ExpFunction();
                    break;
                case "5":
                    problemFunction = new BrownFunction();
                    break;
                case "6":
                    problemFunction = new HappyCatFunction();
                    break;
            }

            problemFunction.ConfigureParameters();
            return problemFunction;
        }

        public static VariableRepresentation VariableInitializer()
        {
            _logger.Info("Provide inforamtion for the variable represenation:");

            _logger.Info("Min range value of the function: ");
            double min = Console.ReadLine().GetDoubleValue();

            _logger.Info("Max range value of the function: ");
            double max = Console.ReadLine().GetDoubleValue();

            _logger.Info("Precision: ");
            int precision = Console.ReadLine().GetIntValue();

            _logger.Info("Deminsion: ");
            int dimension = Console.ReadLine().GetIntValue();

            return new VariableRepresentation(min, max, precision, dimension);
        }
    }
}
