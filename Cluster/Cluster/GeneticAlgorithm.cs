﻿using System;
using System.Collections.Generic;

namespace Cluster
{
    public class GeneticAlgorithm
    {
        private readonly float _mutationRate;
        private readonly Random _random;
        private double _fitValueSum;
        private readonly int _choromosomeSize;
        private readonly Func<bool[], double> _fitnessFunction;
        private readonly Func<bool> _generateRandomGene;
        // some selector for 

        public int Elitism;

        public List<Chromosome> Population { get; private set; } = new List<Chromosome>();
        public List<Chromosome> NewPopulation { get; private set; } = new List<Chromosome>();

        public int GenerationNumber { get; private set; } = 1;
        public double BestFitValue { get; private set; }
        public bool[] BestGenes { get; private set; }

        #region Constructor

        public GeneticAlgorithm(
            int populationSize,
            int chromosomeSize,
            Random random,
            Func<bool> generateRandomGene,
            Func<bool[], double> fitnessFunction,
            int elitism,
            float mutationRate = 0.01f,
            int maxGenerationNumber = 0)
        {
            _mutationRate = mutationRate;
            _random = random;
            _choromosomeSize = chromosomeSize;
            _fitnessFunction = fitnessFunction;
            _generateRandomGene = generateRandomGene;

            for (int i = 0; i < populationSize; i++)
            {
                Population.Add(new Chromosome(fitnessFunction, chromosomeSize, _random));
            }
        }

        #endregion Constructor

        #region Public Methods

        public void CreateGenereation(int numberOfNewChromosome = 0, bool crossoverNewChromosome = false)
        {
            int finalCount = Population.Count + GenerationNumber;

            if(finalCount <= 0)
            {
                return;
            }

            if (Population.Count > 0)
            {
                CalcilateFitness();
                Population.Sort(Chromosome.CompareChormosomes);
            }

            NewPopulation.Clear();

            for (int i = 0; i < Population.Count; i++)
            {
                if (i < Population.Count || crossoverNewChromosome)
                {
                    var parent1 = ChooseParent();
                    var parent2 = ChooseParent();

                    var child = parent1.Crossover(parent2);

                    child.Mutate(_mutationRate);

                    NewPopulation.Add(child);
                }
                else
                {
                    NewPopulation.Add(new Chromosome(_fitnessFunction, _choromosomeSize, _random));
                }
            }

            var tmpList = Population;
            Population = NewPopulation;
            NewPopulation = tmpList;

            GenerationNumber++;
        }

        #endregion Public Methods

        #region Private Methods

        private void CalcilateFitness()
        {
            var bestGene = Population[0];

            for (int i = 0; i < Population.Count; i++)
            {
                _fitValueSum += Population[i].CalculateFintess();

                // TODO: add double comparer with mantissa because it's not valid check
                if (Math.Abs(Population[i].FitValue) < Math.Abs(bestGene.FitValue))
                {
                    bestGene = Population[i];
                }
            }

            BestFitValue = bestGene.FitValue;
            bestGene.Genes.CopyTo(BestGenes, 0);
        }

        private Chromosome ChooseParent()
        {
            double randomNumber = _random.NextDouble() * _fitValueSum;

            for (int i = 0; i < Population.Count; i++)
            {
                if (randomNumber < Population[i].FitValue)
                {
                    return Population[i];
                }

                randomNumber -= Population[i].FitValue;
            }

            return null;
        }

        #endregion Private Methods
    }
}
