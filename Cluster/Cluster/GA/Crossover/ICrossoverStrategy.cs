﻿using System.Collections.Generic;

namespace Cluster.GA.Crossover
{
    interface ICrossoverStrategy
    {
        IList<Chromosome> CrossoverChromosomes(Chromosome firstChromosmoe, Chromosome secondChromosome);
    }
}
