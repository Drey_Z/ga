﻿using System;
using System.Collections.Generic;

namespace Cluster.GA.Crossover
{
    internal class RandomCrossover : ICrossoverStrategy
    {
        private Random _random;
        public RandomCrossover()
        {
            _random = new Random();
        }

        public IList<Chromosome> CrossoverChromosomes(Chromosome firstChromosmoe, Chromosome secondChromosome)
        {
            bool[] firstGenes = new bool[firstChromosmoe.Genes.Length];
            bool[] secondGenes = new bool[secondChromosome.Genes.Length];

            for (int i = 0; i < firstGenes.Length; i++)
            {
                if (_random.NextDouble() >= 0.5)
                {
                    firstGenes[i] = firstChromosmoe.Genes[i];
                    secondGenes[i] = secondChromosome.Genes[i];
                }
                else
                {
                    firstGenes[i] = secondChromosome.Genes[i];
                    secondGenes[i] = firstChromosmoe.Genes[i];
                }

            }

            return new List<Chromosome>
            {
                new Chromosome(firstChromosmoe.FitFunction, firstGenes.Length, _random, firstGenes),
                new Chromosome(secondChromosome.FitFunction, secondGenes.Length, _random, secondGenes)
            };
        }
    }
}
