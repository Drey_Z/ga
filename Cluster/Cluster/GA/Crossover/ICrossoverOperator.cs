﻿using System.Collections.Generic;

namespace Cluster.GA.Crossover
{
    interface ICrossoverOperator
    {
        List<Chromosome> CrossoverToNewPopulation(IList<Chromosome> mateChromosomes, int newPopulationCount);
    }
}
