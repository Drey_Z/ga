﻿using System;
using System.Collections.Generic;

namespace Cluster.GA.Crossover
{
    internal class DomainCrossover : ICrossoverStrategy
    {
        private Random _random;
        public DomainCrossover()
        {
            _random = new Random();
        }

        public IList<Chromosome> CrossoverChromosomes(Chromosome firstChromosmoe, Chromosome secondChromosome)
        {
            bool[] firstGenes = new bool[firstChromosmoe.Genes.Length];
            bool[] secondGenes = new bool[secondChromosome.Genes.Length];

            int startDomainIndex = 0;

            for (int i = 0; i < Chromosome.__VariableRepresentation.Dimension; i++)
            {
                startDomainIndex = i * Chromosome.__VariableRepresentation.VaribaleChromosomeLength;

                if (_random.NextDouble() >= 0.5)
                {
                    Array.Copy(firstChromosmoe.Genes, startDomainIndex, firstGenes, startDomainIndex, Chromosome.__VariableRepresentation.VaribaleChromosomeLength);
                    Array.Copy(secondChromosome.Genes, startDomainIndex, secondGenes, startDomainIndex, Chromosome.__VariableRepresentation.VaribaleChromosomeLength);
                }
                else
                {
                    Array.Copy(firstChromosmoe.Genes, startDomainIndex, secondGenes, startDomainIndex, Chromosome.__VariableRepresentation.VaribaleChromosomeLength);
                    Array.Copy(secondChromosome.Genes, startDomainIndex, firstGenes, startDomainIndex, Chromosome.__VariableRepresentation.VaribaleChromosomeLength);

                }
            }

            return new List<Chromosome>
            {
                new Chromosome(firstChromosmoe.FitFunction, firstGenes.Length, _random, firstGenes),
                new Chromosome(secondChromosome.FitFunction, secondGenes.Length, _random, secondGenes)
            };
        }
    }
}
