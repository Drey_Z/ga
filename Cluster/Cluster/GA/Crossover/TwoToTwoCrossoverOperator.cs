﻿using System;
using System.Collections.Generic;

namespace Cluster.GA.Crossover
{
    internal class TwoToTwoCrossoverOperator : ICrossoverOperator
    {
        private readonly Random _random;
        private ICrossoverStrategy _crossoverStartegy;
        public TwoToTwoCrossoverOperator(ICrossoverStrategy crossoverStartegy)
        {
            _random = new Random();
            _crossoverStartegy = crossoverStartegy;
        }

        public List<Chromosome> CrossoverToNewPopulation(IList<Chromosome> mateChromosomes, int count)
        {
            var newPopulation = new List<Chromosome>();
            if (count != mateChromosomes.Count)
                ScaleCrossover(mateChromosomes, count, newPopulation);
            else
                PairCrossover(mateChromosomes, newPopulation);

            return newPopulation;
        }

        private void PairCrossover(IList<Chromosome> mateChromosomes, List<Chromosome> newPopulation)
        {
            for (int i = 0; i < mateChromosomes.Count / 2; i++)
            {
                var crossoveredChromosomes = _crossoverStartegy.CrossoverChromosomes(mateChromosomes[i], mateChromosomes[i + 1]);

                newPopulation.AddRange(crossoveredChromosomes);
            }

            if (mateChromosomes.Count % 2 != 0)
            {
                RandParentCrossover(mateChromosomes, newPopulation);
            }
        }

        private void ScaleCrossover(IList<Chromosome> mateChromosomes, int count, List<Chromosome> newPopulation)
        {
            for (int i = 0; i < count / 2; i++)
            {
                RandParentCrossover(mateChromosomes, newPopulation);
            }

            if (count % 2 != 0)
            {
                RandParentCrossover(mateChromosomes, newPopulation);
            }
        }

        private void RandParentCrossover(IList<Chromosome> mateChromosomes, List<Chromosome> newPopulation)
        {
            var firstChromosome = mateChromosomes[_random.Next(0, mateChromosomes.Count)];
            var secondChromosmoe = mateChromosomes[_random.Next(0, mateChromosomes.Count)];

            var crossoveredChromosomes = _crossoverStartegy.CrossoverChromosomes(firstChromosome, secondChromosmoe);

            newPopulation.AddRange(crossoveredChromosomes);
        }
    }
}
