﻿using System.Collections.Generic;

namespace Cluster.GA.Mutation
{
    interface IMutationOperator
    {
        void MutatePopulation(IList<Chromosome> popilation);
    }
}
