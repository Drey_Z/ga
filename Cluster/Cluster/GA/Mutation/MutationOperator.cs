﻿using System;
using System.Collections.Generic;

namespace Cluster.GA.Mutation
{
    class MutationOperator : IMutationOperator
    {
        private Random _random;

        private readonly int _mutationRate = 0;
        public MutationOperator(int mutationRate)
        {
            _mutationRate = mutationRate;
            _random = new Random();
        }

        public void MutatePopulation(IList<Chromosome> popilation)
        {
            foreach (var chromosome in popilation)
            {
                PerformMuatation(chromosome);
            }
        }

        private void PerformMuatation(Chromosome chromosome)
        {
            for (int i = 0; i < _mutationRate; i++)
            {
                var index = _random.Next(0, chromosome.Genes.Length);
                chromosome.Genes[index] = !chromosome.Genes[index];
            }
        }
    }
}
