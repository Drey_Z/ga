﻿using System.Collections.Generic;

namespace Cluster.GA.Selection
{
    interface ISelectionStrategy<T>
        where T : IFit
    {
        List<T> Selection(List<T> Population);
    }
}
