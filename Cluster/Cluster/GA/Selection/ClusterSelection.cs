﻿using Cluster.Modifications;
using NLog;
using System.Collections.Generic;
using System.Linq;

namespace Cluster.GA.Selection
{
    internal class ClusterSelection : ISelectionStrategy<Chromosome>
    {
        private static readonly ILogger _logger = LogManager.GetCurrentClassLogger();

        private ISelectionStrategy<ClusterEntity> _clusterStrategySelection;
        private ISelectionStrategy<Chromosome> _chromosomeStrategySelection;
        private IClusterSelector _clusterStrategy;

        public ClusterSelection(
            IClusterSelector clusterStrategy,
            ISelectionStrategy<ClusterEntity> clusterSelectionStrategy,
            ISelectionStrategy<Chromosome> chromosomeStrategySelection = null)
        {
            _clusterStrategySelection = clusterSelectionStrategy;
            _chromosomeStrategySelection = chromosomeStrategySelection;
            _clusterStrategy = clusterStrategy;
        }

        public List<Chromosome> Selection(List<Chromosome> Population)
        {
            _logger.Debug("Performing clusterisation");
            var clustering = new ChromosomeClusterSelector(Population);
            clustering.Clusterize();

            var bestChromosomesFromClusters = _clusterStrategySelection.Selection(clustering.Clusters).Select(c => c.BestChromosome).ToList(); ;

            bestChromosomesFromClusters = _chromosomeStrategySelection?.Selection(bestChromosomesFromClusters);

            return bestChromosomesFromClusters;
        }
    }
}
