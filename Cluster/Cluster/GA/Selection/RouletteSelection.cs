﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Cluster.GA.Selection
{
    internal class RouletteSelection<T> : ISelectionStrategy<T> where T : IFit
    {
        private static readonly ILogger _logger = LogManager.GetCurrentClassLogger();
        private readonly Random _random;
        private readonly bool _maximiazation;

        public RouletteSelection(bool maximization)
        {
            _random = new Random();
            _maximiazation = maximization;
        }

        public List<T> Selection(List<T> Population)
        {
            var selectedChromosomes = new List<T>();
            var rouleteRepresenation = _maximiazation ? GetRouleteValues(Population) : GetMinRouleteValues(Population);

            for (int i = 0; i < Population.Count; i++)
            {
                double p = _random.NextDouble();
                var selectedChromosome = rouleteRepresenation.First(c => c.BeginRange <= p && p <= c.EndRane).Chromosome;
                selectedChromosomes.Add(selectedChromosome);
            }

            return selectedChromosomes;
        }

        private List<(double BeginRange, double EndRane, T Chromosome)> GetRouleteValues(List<T> Population)
        {
            double fitSum = Population.Sum(p => p.FitValue);
            var roulete = new List<(double BeginRange, double EndRane, T BestRepresentor)>();
            double previousValue = 0;
            for (int i = 0; i < Population.Count; i++)
            {
                double probability = Population[i].FitValue / fitSum;
                double endRange = previousValue + probability;
                roulete.Add((BeginRange: previousValue, EndRane: endRange, Population[i]));

                previousValue += probability;
            }

            return roulete;
        }

        private List<(double BeginRange, double EndRane, T Chromosome)> GetMinRouleteValues(List<T> Population)
        {
            double fitSum = Population.Sum(p => p.FitValue);

            if (fitSum == 0)
            {
                return SelectTheBest(Population);
            }

            double scaledSum = Population.Sum(p => fitSum - p.FitValue);

            var roulete = new List<(double BeginRange, double EndRane, T BestRepresentor)>();
            double previousValue = 0;
            for (int i = 0; i < Population.Count; i++)
            {
                double probability = (fitSum - Population[i].FitValue) / scaledSum;
                double endRange = previousValue + probability;
                roulete.Add((BeginRange: previousValue, EndRane: endRange, Population[i]));

                previousValue += probability;
            }

            return roulete;
        }

        private static List<(double BeginRange, double EndRane, T IFitEnitity)> SelectTheBest(List<T> Population)
        {
            double best = Population.Min(t => t.FitValue);
            var bestChromosome = Population.First(t => t.FitValue == best);
            return new List<(double BeginRange, double EndRane, T BestRepresentor)>()
                {
                    (BeginRange: 0.0, EndRane: 1.0, bestChromosome)
                };
        }
    }
}
