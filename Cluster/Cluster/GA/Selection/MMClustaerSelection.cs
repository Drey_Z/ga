﻿using Cluster.Modifications;
using System;
using System.Collections.Generic;

namespace Cluster.Extentions.Selection
{
    internal class MMClustaerSelection : ISelectionStrategy<Chromosome>
    {
        private ISelectionStrategy<ClusterEntity> _clusterStrategySelection;
        private ISelectionStrategy<Chromosome> _chromosomeStrategySelection;

        public MMClustaerSelection(
            ISelectionStrategy<Chromosome> chromosomeStrategySelection,
            ISelectionStrategy<ClusterEntity> clusterSelectionStrategy)
        {
            _clusterStrategySelection = clusterSelectionStrategy;
            _chromosomeStrategySelection = chromosomeStrategySelection;
        }

        public List<Chromosome> Selection(List<Chromosome> Population)
        {
            Console.WriteLine("Performing clusterisation");
            var clustering = new ChromosomeClusterSelector(Population);

            var selectedChrosmomes = _clusterStrategySelection.Selection(clustering.Clusters);


            return selectedChrosmomes;
        }
    }
}
