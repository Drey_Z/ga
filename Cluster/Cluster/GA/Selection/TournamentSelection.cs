﻿using Cluster.Extentions;
using NLog;
using System;
using System.Collections.Generic;

namespace Cluster.GA.Selection
{
    public class TournamentSelection : ISelectionStrategy<Chromosome>
    {
        private static readonly ILogger _logger = LogManager.GetCurrentClassLogger();
        private Random _random;
        public int _selectionPressure;

        public TournamentSelection()
        {
            _random = new Random();
            ConsoleConfiguration();
        }

        public void ConsoleConfiguration()
        {
            _logger.Info("Set selection pressure: ");
            _selectionPressure = Console.ReadLine().GetIntValue();
        }

        public List<Chromosome> Selection(List<Chromosome> Population)
        {
            var selectionList = new List<Chromosome>(Population.Count);

            for (int i = 0; i < Population.Count; i++)
            {
                selectionList.Add(Tournament(Population));
            }

            return selectionList;
        }

        private Chromosome Tournament(List<Chromosome> population)
        {
            var tournamentList = new List<Chromosome>(_selectionPressure);

            for (int i = 0; i < _selectionPressure; i++)
            {
                tournamentList.Add(population[_random.Next(0, population.Count)]);
            }

            tournamentList.Sort(Chromosome.__Comparer);
            return tournamentList[0];
        }
    }
}
