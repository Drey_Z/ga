﻿using Cluster.Benchmarks;
using Cluster.GA.Crossover;
using Cluster.GA.Mutation;
using Cluster.GA.Selection;
using NLog;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Cluster.GA
{
    internal class GeneticAlgorithm
    {
        private static readonly ILogger _logger = LogManager.GetCurrentClassLogger();

        private List<Chromosome> Population { get; set; } = new List<Chromosome>();
        public List<Chromosome> BestChromosomes { get; set; }
        internal ISelectionStrategy<Chromosome> SelectionStrategy { get; set; }

        private Stopwatch _stopwatch = new Stopwatch();

        private int _populationSize;
        private int _generationNumber;
        private IMutationOperator _mutationOperator;
        private ICrossoverOperator _crossoverOperator;
        private IProblemFunction _problemFunction;
        private Random _random;
        private bool _mesureOperators;

        public GeneticAlgorithm(
            int populationSize,
            ISelectionStrategy<Chromosome> selectionStrategy,
            ICrossoverOperator crossoverOperator,
            IMutationOperator mutationOperator,
            IProblemFunction problemFunction,
            bool mesureOperators = false)
        {
            _populationSize = populationSize;
            SelectionStrategy = selectionStrategy;
            _crossoverOperator = crossoverOperator;
            _mutationOperator = mutationOperator;
            _problemFunction = problemFunction;
            _generationNumber = 0;
            _mesureOperators = mesureOperators;
            BestChromosomes = new List<Chromosome>();
            Chromosome.__VariableRepresentation = problemFunction.VariableRepresentation;

            _random = new Random();
        }

        public void Reset()
        {
            Population.Clear();
            BestChromosomes.Clear();

            _generationNumber = 0;
        }

        public void StartGA(int maxGeneration)
        {
            GeneratePopulation();

            _logger.Info("Starting genetic algorithm with maximum generation.");
            StartWithGenerationLimit(maxGeneration);
        }

        private void GeneratePopulation()
        {
            for (int i = 0; i < _populationSize; i++)
            {
                Population.Add(new Chromosome(_problemFunction.FitnessFunction, _problemFunction.VariableRepresentation.ChromosomeLength, _random));
            }

            CopmuteFitness();
        }

        private void StartWithGenerationLimit(int maxGeneration)
        {
            for (_generationNumber = 0; _generationNumber < maxGeneration; _generationNumber++)
            {
                if (_mesureOperators)
                    ProcessGenerationWithStopwatch();
                else
                    ProcessGeneration();
            }
        }

        private void ProcessGenerationWithStopwatch()
        {
            _stopwatch.Start();
            var selectionMate = SelectionStrategy.Selection(Population);
            _stopwatch.Stop();
            LogOperator("Selection");

            _stopwatch.Restart();
            Population = _crossoverOperator.CrossoverToNewPopulation(selectionMate, _populationSize);
            _stopwatch.Stop();
            LogOperator("Crossover");


            _stopwatch.Restart();
            _mutationOperator.MutatePopulation(Population);
            _stopwatch.Stop();
            LogOperator("Mutation");

            _stopwatch.Restart();
            CopmuteFitness();
            _stopwatch.Stop();
            LogOperator("Fit");

        }

        private void LogOperator(string operation)
        {
            _logger.Warn("Operation: {op}: ms: {ms}, tic: {tic}.", operation, _stopwatch.ElapsedMilliseconds, _stopwatch.ElapsedTicks);
        }

        private void ProcessGeneration()
        {
            var selectionMate = SelectionStrategy.Selection(Population);
            Population = _crossoverOperator.CrossoverToNewPopulation(selectionMate, _populationSize);
            _mutationOperator.MutatePopulation(Population);
            CopmuteFitness();
        }

        private void CopmuteFitness()
        {
            foreach (var chromosome in Population)
            {
                chromosome.CalculateFintess();
            }

            _logger.Debug("\n");
            Population.Sort(Chromosome.__Comparer);

            _logger.Debug("Fit Values: ");
            foreach (var chromsome in Population)
                _logger.Debug("{0} ", chromsome.FitValue);

            _logger.Debug("\n");
            _logger.Debug("\n");

            Population[0].Generation = _generationNumber;
            BestChromosomes.Add(Population[0]);
        }
    }
}
