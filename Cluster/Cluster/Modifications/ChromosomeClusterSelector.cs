﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Cluster.Modifications
{
    public class ClusterEntity : IFit
    {
        public List<Chromosome> ClusterChromosomes { get; set; } = new List<Chromosome>();

        public Chromosome Center { get; set; }

        public Chromosome BestChromosome
        {
            get
            {
                var list = new List<Chromosome>(ClusterChromosomes);
                list.Add(Center);
                list.Sort(Chromosome.__Comparer);

                return list[0];
            }
        }

        public double FitValue => BestChromosome.FitValue;
    }

    internal interface IClusterSelector
    {
        List<ClusterEntity> Clusters { get; set; }
        void Clusterize();
    }

    internal class ChromosomeClusterSelector
    {

        private List<Chromosome> _chromosomes = new List<Chromosome>();

        public List<ClusterEntity> Clusters { get; set; }

        public ChromosomeClusterSelector(List<Chromosome> chromosomes)
        {
            _chromosomes = chromosomes;

        }

        public void Clusterize()
        {
            CalculateCoordinates(_chromosomes);
            CalculateClusters();
        }

        private void CalculateCoordinates(List<Chromosome> chromosomes)
        {
            foreach (var chormosome in chromosomes)
            {
                chormosome.CalculateCoordinates();
            }
        }

        private void CalculateClusters()
        {
            var clusters = new List<ClusterEntity>();

            var firstCluster = new ClusterEntity();
            var secondCluster = new ClusterEntity();

            var maxPair = FindCenterPoint();
            
            firstCluster.Center = maxPair.FirstPoint;
            secondCluster.Center = maxPair.SecondPoit;

            clusters.Add(firstCluster);
            clusters.Add(secondCluster);

            FillOtherClusterts(clusters, maxPair.distace);

            Clusters = clusters;
        }

        private (Chromosome FirstPoint, Chromosome SecondPoit, double distace) FindCenterPoint()
        {
            var generalList = new List<(Chromosome FirstPoint, Chromosome SecondPoit, double Distance)>();
            foreach(var point in _chromosomes)
            {
                generalList.AddRange(CalculateDistanceForOtherPoints(
                point,
                _chromosomes.Except(new List<Chromosome> { point })).ToList());
            }

            return generalList.Where(t => t.Distance == generalList.Max(d => d.Distance)).FirstOrDefault();
        }

        private void FillOtherClusterts(List<ClusterEntity> clusters, double distance)
        {
            List<double> distances = new List<double>() { distance };

            while (true)
            {
                var otherPoints = _chromosomes.Except(clusters.Select(c => c.Center).ToList()).ToList();

                if (otherPoints.Count == 0)
                    return;
                var maxminPair = GetMaxFromMin(otherPoints, clusters.Select(t => t.Center).ToList());

                if (IsFinnished(distances, maxminPair.disnace))
                {
                    FillClusters(clusters, otherPoints);
                    break;
                }
                else
                {
                    var cluster = new ClusterEntity();
                    cluster.Center = maxminPair.poissibleCenter;
                    clusters.Add(cluster);
                    distances.Add(maxminPair.disnace);
                }
            }

        }

        private void FillClusters(List<ClusterEntity> clusters, List<Chromosome> otherPoints)
        {
            foreach (var point in otherPoints)
            {
                var disnacesPairs = CalculateDistanceForOtherPoints(point, clusters.Select(t => t.Center).ToList());
                var minDistancePair = disnacesPairs.First(p => p.Distance == disnacesPairs.Min(d => d.Distance));

                clusters.First(p => p.Center == minDistancePair.SecondPoint).ClusterChromosomes.Add(minDistancePair.FirstPoint);
            }
        }

        private (Chromosome center, Chromosome poissibleCenter, double disnace) GetMaxFromMin(List<Chromosome> otherPoints, List<Chromosome> clusters)
        {
            var pairs = new List<(Chromosome center, Chromosome poissibleCenter, double disnace)>();

            foreach (var point in otherPoints)
            {
                var disnacesPairs = CalculateDistanceForOtherPoints(point, clusters);
                var minPair = disnacesPairs.First(p => p.Distance == disnacesPairs.Min(d => d.Distance));
                pairs.Add((center: minPair.SecondPoint, poissibleCenter: minPair.FirstPoint, disnace: minPair.Distance));
            }

            return pairs.First(p => p.disnace == pairs.Max(d => d.disnace));
        }

        private bool IsFinnished(List<double> distances, double maxminDistance)
        {
            return maxminDistance < distances.Average() / 2;
        }

        private List<(Chromosome FirstPoint, Chromosome SecondPoint, double Distance)> CalculateDistanceForOtherPoints(Chromosome center, IEnumerable<Chromosome> otherPoints)
        {
            var resultList = new List<(Chromosome FirstPoint, Chromosome SecondPoint, double Distance)>();

            var centerCoordinates = center.Coordinates;

            foreach (var otherPoint in otherPoints)
            {
                var otherPointCoordinates = otherPoint.Coordinates;
                double distanceBetween = CalculateDistance(centerCoordinates, otherPointCoordinates);
                resultList.Add((FirstPoint: center, SecondPoint: otherPoint, Distance: distanceBetween));
            }

            return resultList;
        }

        private double CalculateDistance(double[] centerCoordinates, double[] otherPointCoordinates)
        {
            double distance = 0;
            for (int i = 0; i < centerCoordinates.Length; i++)
            {
                distance += Math.Pow(centerCoordinates[i] - otherPointCoordinates[i], 2);
            }

            return Math.Sqrt(distance);
        }

    }
}
