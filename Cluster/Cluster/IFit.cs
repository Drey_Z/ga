﻿namespace Cluster
{
    interface IFit
    {
        double FitValue { get; }
    }
}
