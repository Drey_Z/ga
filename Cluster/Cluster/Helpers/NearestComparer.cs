﻿using System;
using System.Collections.Generic;

namespace Cluster.Helpers
{
    public class NearestComparer : IComparer<Chromosome>
    {
        private double _targetValue;

        public NearestComparer(double targetValue)
        {
            _targetValue = targetValue;
        }

        public int Compare(Chromosome firtsChromosome, Chromosome secondChromosome)
        {
            double distanceToFirst = Math.Abs(_targetValue - firtsChromosome.FitValue);
            double distanceToSecond = Math.Abs(_targetValue - secondChromosome.FitValue);

            if (distanceToFirst > distanceToSecond)
            {
                return 1;
            }
            else if (distanceToFirst < distanceToSecond)
            {
                return -1;
            }
            else
            {
                return 0;
            }
        }
    }
}
