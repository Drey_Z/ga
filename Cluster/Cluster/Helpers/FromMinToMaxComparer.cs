﻿using System.Collections.Generic;

namespace Cluster.Helpers
{
    public class FromMinToMaxComparer : IComparer<Chromosome>
    {
        public int Compare(Chromosome firtsChromosome, Chromosome secondChromosome)
        {
            if (firtsChromosome.FitValue > secondChromosome.FitValue)
            {
                return 1;
            }
            else if (firtsChromosome.FitValue < secondChromosome.FitValue)
            {
                return -1;
            }
            else
            {
                return 0;
            }
        }
    }
}
